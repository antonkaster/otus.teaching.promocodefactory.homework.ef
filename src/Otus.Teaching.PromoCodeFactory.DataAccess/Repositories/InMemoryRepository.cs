﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> filterFunc)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<TResult>> GetAsync<TResult>(Expression<Func<T, bool>> filterFunc, Expression<Func<T, TResult>> selectFunc)
        {
            throw new NotImplementedException();
        }

        public Task CreateAsync(T itemToCreate)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(T itemToUpdate)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(T itemToDelete)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<TResult>> GetAsync<TResult>(Expression<Func<T, TResult>> selectFunc)
        {
            throw new NotImplementedException();
        }
    }
}