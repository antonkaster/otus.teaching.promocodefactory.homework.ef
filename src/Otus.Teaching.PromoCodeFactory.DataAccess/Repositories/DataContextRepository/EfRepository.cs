﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.DataContextRepository
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private PromoDbContext _db;

        public EfRepository(PromoDbContext promoDb)
        {
            _db = promoDb ?? throw new ArgumentNullException(nameof(promoDb));

        }

        public async virtual Task<IEnumerable<T>> GetAllAsync()
        {
            return _db.Set<T>();
        }

        public async virtual Task<IEnumerable<T>> GetAsync(Expression<Func<T,bool>> filterFunc)
        {
            return _db.Set<T>()
                .Where(filterFunc);
        }

        public async Task<IEnumerable<TResult>> GetAsync<TResult>(Expression<Func<T, TResult>> selectFunc)
        {
            return _db.Set<T>()
                .Select(selectFunc);
        }

        public async virtual Task<IEnumerable<TResult>> GetAsync<TResult>(
            Expression<Func<T,bool>> filterFunc, 
            Expression<Func<T, TResult>> selectFunc
            )
        {
            return _db.Set<T>()
                .Where(filterFunc)
                .Select(selectFunc);
        }

        public async virtual Task<T> GetByIdAsync(Guid id)
        {
            return _db.Set<T>()
                .Where(e => e.Id == id)
                .FirstOrDefault();
        }

        public async virtual Task CreateAsync(T itemToCreate)
        {
            _db.Add(itemToCreate);
            _db.SaveChanges();
        }

        public async virtual Task UpdateAsync(T itemToUpdate)
        {
            _db.Entry(itemToUpdate).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _db.SaveChanges();
        }

        public async virtual Task DeleteAsync(T itemToDelete)
        {
            _db.Entry(itemToDelete).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
            _db.SaveChanges();
        }

    }
}
