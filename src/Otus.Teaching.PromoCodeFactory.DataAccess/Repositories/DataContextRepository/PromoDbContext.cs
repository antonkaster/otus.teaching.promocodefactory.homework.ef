﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.DataContextRepository
{
    public class PromoDbContext : DbContext
    {
        public DbSet<Role> Roles { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public PromoDbContext(DbContextOptions<PromoDbContext>  dbContextOptions) : base(dbContextOptions)
        {
            //Database.EnsureDeleted();

            if(Database.EnsureCreated())
                FillInitData();
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>()
                .HasKey(r => r.Id);
            modelBuilder.Entity<Role>()
                .Property(r => r.Name)
                .HasMaxLength(1024);
            modelBuilder.Entity<Role>()
                .Property(r => r.Description)
                .HasMaxLength(1024);

            modelBuilder.Entity<Employee>()
                .HasKey(e => e.Id);
            modelBuilder.Entity<Employee>()
                .HasOne(e => e.Role);
            modelBuilder.Entity<Employee>()
                .Property(e => e.FirstName)
                .HasMaxLength(1024);
            modelBuilder.Entity<Employee>()
                .Property(e => e.LastName)
                .HasMaxLength(1024);
            modelBuilder.Entity<Employee>()
                .Property(e => e.Email)
                .HasMaxLength(1024);

            modelBuilder.Entity<PromoCode>()
                .HasKey(r => r.Id);
            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.Preference);
            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.PartnerManager);
            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.Customer)
                .WithMany(c => c.PromoCodes)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<PromoCode>()
                .Property(p => p.Code)
                .HasMaxLength(1024);
            modelBuilder.Entity<PromoCode>()
                .Property(p => p.ServiceInfo)
                .HasMaxLength(1024);
            modelBuilder.Entity<PromoCode>()
                .Property(p => p.PartnerName)
                .HasMaxLength(1024);

            modelBuilder.Entity<CustomerPreference>()
                .HasKey(cp => cp.Id);
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Customer)
                .WithMany(cp => cp.Preferences)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Customer>()
                .HasKey(c => c.Id);
            modelBuilder.Entity<Customer>()
                .Property(c => c.FirstName)
                .HasMaxLength(1024);
            modelBuilder.Entity<Customer>()
                .Property(c => c.LastName)
                .HasMaxLength(1024);
            modelBuilder.Entity<Customer>()
                .Property(c => c.Email)
                .HasMaxLength(1024);

            modelBuilder.Entity<Preference>()
                .HasKey(p => p.Id);
            modelBuilder.Entity<Preference>()
                .HasMany(p => p.Customers)
                .WithOne(cp => cp.Preference);
            modelBuilder.Entity<Preference>()
                .Property(p => p.Name)
                .HasMaxLength(1024);
        }

        private void FillInitData()
        {
            Roles.AddRange(FakeDataFactory.Roles);
            Preferences.AddRange(FakeDataFactory.Preferences);
            SaveChanges();

            foreach (var empl in FakeDataFactory.Employees)
            {
                empl.Role = Roles.FirstOrDefault(r => r.Id == empl.Role.Id);
                Employees.Add(empl);
            }
            
            foreach (var cust in FakeDataFactory.Customers)
            {
                foreach (var pref in cust.Preferences)
                    pref.Preference = Preferences.FirstOrDefault(p => p.Id == pref.Preference.Id);
                Customers.Add(cust);
            }
            
            SaveChanges();
        }
    }
}
