﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PreferenceController : ControllerBase
    {
        private readonly IRepository<Preference> _prefs;

        public PreferenceController(
            IRepository<Preference> prefs
            )
        {
            _prefs = prefs ?? throw new ArgumentNullException(nameof(_prefs));
        }


        /// <summary>
        /// Получить всех предпочтений
        /// </summary>
        /// <returns>Список всех предпочтений</returns>
        [HttpGet]
        public async Task<ActionResult<List<PrefernceResponse>>> GetCustomersAsync()
        {
            var prefsList = (await _prefs.GetAllAsync())
                .Select(p => new PrefernceResponse()
                {
                    Id = p.Id,
                    Name = p.Name
                });

            return prefsList.ToList();
        }
    }
}
