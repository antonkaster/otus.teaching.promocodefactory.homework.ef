﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.DataContextRepository;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {

        private readonly IRepository<Customer> _customers;
        private readonly IRepository<PromoCode> _promoCodes;
        private readonly IRepository<CustomerPreference> _customerPrefs;
        private readonly IRepository<Preference> _prefs;

        public CustomersController(
            IRepository<Customer> customersRepository, 
            IRepository<PromoCode> promoCodeRepository,
            IRepository<CustomerPreference> customerPrefsRepository,
            IRepository<Preference> prefs
            )
        {
            _customers = customersRepository ?? throw new ArgumentNullException(nameof(customersRepository));
            _promoCodes = promoCodeRepository ?? throw new ArgumentNullException(nameof(promoCodeRepository));
            _customerPrefs = customerPrefsRepository ?? throw new ArgumentNullException(nameof(customerPrefsRepository));
            _prefs = prefs ?? throw new ArgumentNullException(nameof(_prefs));
        }

        /// <summary>
        /// Получить всех клиентов
        /// </summary>
        /// <returns>Список всех клиентов</returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customersList =  (await _customers.GetAllAsync())
                .Select(c => new CustomerShortResponse()
                { 
                    Id = c.Id,
                    FirstName = c.FirstName,
                    LastName = c.LastName,
                    Email = c.Email
                });

            return customersList.ToList();
        }
        
        /// <summary>
        /// Получить информацию о клиенте
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <returns>Информация о клиенте</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync([Required] Guid id)
        {
            var customer = await _customers.GetByIdAsync(id);
            if (customer == null)
                return NotFound("Customer not found!");

            return new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = (await _customerPrefs.GetAsync(cp => cp.Customer == customer, cp => cp.Preference))
                    .Select(p => new PrefernceResponse() 
                    { 
                        Id = p.Id,
                        Name = p.Name
                    })
                    .ToList(),
                PromoCodes = (await _promoCodes.GetAsync(c => c.Customer == customer))
                    .Select(c => new PromoCodeShortResponse()
                    {
                        Id = c.Id,
                        Code = c.Code,
                        BeginDate = c.BeginDate.ToString(),
                        EndDate = c.EndDate.ToString(),
                        PartnerName = c.PartnerName,
                        ServiceInfo = c.ServiceInfo
                    })
                    .ToList()                    
            };
        }

        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <param name="request">Данные клиента</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync([Required] CreateOrEditCustomerRequest request)
        {
            Customer customer = new Customer()
            {
                Id = Guid.NewGuid(),
                FirstName = request?.FirstName,
                LastName = request?.LastName,
                Email = request?.Email,
            };
            customer.Preferences = request.PreferenceIds
                .Select(p => new CustomerPreference()
                {
                    Id = Guid.NewGuid(),
                    Customer = customer,
                    Preference =  _prefs.GetAsync(p => p.Id == p.Id).Result.FirstOrDefault()
                })
                .ToList();

            await _customers.CreateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Изменить данные клиента
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <param name="request">Данные клиента</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync([Required] Guid id, [Required] CreateOrEditCustomerRequest request)
        {
            var customer = await _customers.GetByIdAsync(id);
            if (customer == null)
                return NotFound("Customer not found!");

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            foreach (var pref in await _customerPrefs.GetAsync(p => p.Customer == customer))
                await _customerPrefs.DeleteAsync(pref);

            customer.Preferences = request.PreferenceIds
                .Select(pref => new CustomerPreference()
                {
                    Customer = customer,
                    Preference = _prefs.GetAsync(p => p.Id == pref).Result.FirstOrDefault()
                })
                .ToList();

            await _customers.UpdateAsync(customer);

            return Ok();
        }


        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer([Required] Guid id)
        {
            var customer = await _customers.GetByIdAsync(id);
            if (customer == null)
                return NotFound("Customer not found!");

            await _customers.DeleteAsync(customer);

            return Ok();
        }
    }
}