﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<IEnumerable<T>> GetAsync(Expression<Func<T,bool>> filterFunc);
        Task<IEnumerable<TResult>> GetAsync<TResult>(Expression<Func<T, TResult>> selectFunc);
        Task<IEnumerable<TResult>> GetAsync<TResult>(Expression<Func<T,bool>> filterFunc, Expression<Func<T, TResult>> selectFunc);
        
        Task<T> GetByIdAsync(Guid id);

        Task CreateAsync(T itemToCreate);
        Task UpdateAsync(T itemToUpdate);
        Task DeleteAsync(T itemToDelete);
    }
}